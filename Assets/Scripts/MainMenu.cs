﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour {

    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void MemoryGameSelection()
    {
        SceneManager.LoadScene(1);
    }
    public void MultiChoiceSelection()
    {
        SceneManager.LoadScene(2);
    }
    public void LibrarySelection()
    {
        SceneManager.LoadScene(3);
    }
    public void MemoryGameLevelOne()
    {
        SceneManager.LoadScene(4);
    }
    public void MemoryGameLevelTwo()
    {
        SceneManager.LoadScene(5);
    }
    public void MemoryGameLevelThree()
    {
        SceneManager.LoadScene(6);
    }

    public void MultiChoiceGameOne()
    {
        SceneManager.LoadScene(8);
    }
    public void MultiChoiceGameTwo()
    {
        SceneManager.LoadScene(9);
    }
    public void MultiChoiceGameThree()
    {
        SceneManager.LoadScene(10);
    }
    public void Quit()
    {
        Application.Quit();
    }

}
