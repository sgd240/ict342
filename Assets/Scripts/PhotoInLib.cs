﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.UI;

public class PhotoInLib : MonoBehaviour
{

    Texture2D texture;
    RawImage image;
    public Button getPhoto;
    public Button addDetails;
    public Image Background;
    public bool hasPhoto;
    public string picSaveName;
    void Start()
    {
        image = GetComponent<RawImage>();
        picSaveName = gameObject.name;
        LoadTexture();
    }

    void Update()
    {
        if (texture == null)
        {
            hasPhoto = false;
        }
        else if (texture)
        {
            hasPhoto = true;
        }
    }

    public void GetImage()
    {
        Debug.Log("Save Name: " + gameObject.name);
        FileBrowser.OpenFilePanel("Open file Title", Environment.GetFolderPath(Environment.SpecialFolder.Desktop), null, null, (bool canceled, string filePath) =>
        {
            if (canceled)
            {
                //m_LabelContent = "[Open File] Canceled";
                Debug.Log("[Open File] Canceled");
                return;
            }
            byte[] saveThis;
            saveThis = System.IO.File.ReadAllBytes(filePath);
            var tex = new Texture2D(60, 60);
            tex.LoadImage(saveThis);
            image.texture = tex;
            
            System.IO.File.WriteAllBytes(Application.dataPath + "/" + picSaveName, tex.EncodeToPNG());
            //System.IO.File.WriteAllBytes(Application.persistentDataPath + "/" + picSaveName, tex.EncodeToPNG());
            // m_LabelContent = "[Open File] Selected file: " + filePath;

        });
    }
    public void LoadTexture()
    {
        try
        {
            Debug.Log("trying to load");
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(Application.dataPath + "/" + picSaveName);
            var loadedTex = new Texture2D(60, 60);
            loadedTex.LoadImage(bytes);
            loadedTex.Apply();
            image.texture = loadedTex;
            gameObject.tag = "HasImage";
        }
        catch (Exception)
        {
            Debug.Log("Image.texture Failed to Load Find Picture");
            throw;
        }
    
    }
  //should spawn an overlay to fill in details 
    public void GetDetails()
    {
        Debug.Log("Spawn Overlay To Fill in Details");
    }
    //These will spawn addition buttons to navigate the photo library
    
    public void OnClick()
    {
        if (!addDetails.gameObject.activeInHierarchy && !getPhoto.gameObject.activeInHierarchy)
        {
         
            addDetails.gameObject.SetActive(true);
            getPhoto.gameObject.SetActive(true);
            Background.gameObject.SetActive(true);
    
        }
        else if (addDetails.gameObject.activeInHierarchy && getPhoto.gameObject.activeInHierarchy)
        {
            addDetails.gameObject.SetActive(false);
            getPhoto.gameObject.SetActive(false);
            Background.gameObject.SetActive(false);
        }
        
    }
    public void Cancel()
    {
        addDetails.gameObject.SetActive(false);
        getPhoto.gameObject.SetActive(false);
        Background.gameObject.SetActive(false);
    }

}
