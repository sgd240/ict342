﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Card : MonoBehaviour
{
    public static bool doNot = false;

    [SerializeField]
    private int state;
    [SerializeField]
    private int cardValue;
    [SerializeField]
    private bool initialized = false;

    private Sprite cardBack;
    private Sprite cardFace;

    private GameObject GameManager;
    private GameObject GameManagerLevelTwo;
    private GameObject GameManagerLevelThree;
    Scene scene;
    void Start()
    {
        scene = SceneManager.GetActiveScene();
        state = 1;
        if (scene.name == "MemoryLevelOne")
        {
            GameManager = GameObject.FindGameObjectWithTag("GameManager");
            Debug.Log("Level One Game Manager Attempt to Load");
        }
        else if (scene.name == "MemoryLevelTwo")
        {
            GameManagerLevelTwo = GameObject.FindGameObjectWithTag("GameManager");
            Debug.Log("Level Two Game Manager Attempt to Load");
        }
        else if (scene.name == "MemoryLevelThree")
        {
            GameManagerLevelThree = GameObject.FindGameObjectWithTag("GameManager");
            Debug.Log("Level Three Game Manager Attempt to Load");
        }
    }
    public void setupGraphics()
    {
        if (scene.name == "MemoryLevelOne")
        {
            cardBack = GameManager.GetComponent<GameController>().getCardBack();
            cardFace = GameManager.GetComponent<GameController>().getCardFace(cardValue);
        }
        else if (scene.name == "MemoryLevelTwo")
        {
            cardBack = GameManagerLevelTwo.GetComponent<GameControllerMemoryLevelTwo>().getCardBack();
            cardFace = GameManagerLevelTwo.GetComponent<GameControllerMemoryLevelTwo>().getCardFace(cardValue);
        }
        //THIS NEEDS TO GET CHANGED TO THE LEVEL THREE GAME CONTROLLER
        else if (scene.name == "MemoryLevelThree")
        {
            cardBack = GameManagerLevelThree.GetComponent<GameControllerMemoryLevelThree>().getCardBack();
            cardFace = GameManagerLevelThree.GetComponent<GameControllerMemoryLevelThree>().getCardFace(cardValue);
        }

        flipCard();
    }

   public void flipCard()
    {
        
        Debug.Log("Flip Card");
        if (state == 0)
        {
            state = 1;      
        }
        else if (state == 1 )
        {
            state = 0;
        }
        if (state == 0 && !doNot)
        {
            GetComponent<Image>().sprite = cardBack;
        }
        else if (state == 1 && !doNot)
        {
            GetComponent<Image>().sprite = cardFace;
        }
     
    }
    public int CardValue
    {
        get { return cardValue; }
        set { cardValue = value; }
    }

    public int State
    {
        get { return state; }
        set { state = value; }

    }
    public bool Init
    {
        get { return initialized; }
        set { initialized = value; }
    }
    public void falseCheck()
    {
        StartCoroutine(pause());
    }
    IEnumerator pause()
    {
        Debug.Log("Pause Triggers");
        yield return new WaitForSeconds(1);
        if (state == 0)
        {
            GetComponent<Image>().sprite = cardBack;
        }
        else if (state == 1)
        {
            GetComponent<Image>().sprite = cardFace;
        }
        doNot = false;
    }

}



