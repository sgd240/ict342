﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameControllerMemoryLevelTwo : MonoBehaviour
{


    //https://www.youtube.com/watch?v=AaQltIC6jqg&index=4&list=PLbCx65TBvT-RHOTE_Lt7iTAev-4Bf84Fu
    public Sprite[] cardFace;
    public Image[] picturesLoaded;
    public Sprite cardBack;
    public GameObject[] cards;

    bool onlyOnce;
    public Text MatchText;
    private bool init = false;
    private int matches = 12;
    float cooldown = .5f;
    private void Start()
    {
        
    }
    void Update()
    {
        if (!init)
        {
            initializeCards();
        }
        cooldown -= Time.deltaTime;
        if (Input.GetMouseButtonUp(0))
        {
            checkCards();
        }
    }
    void initializeCards()
    {
        for (int id = 0; id < 2; id++)
        {
            for (int i = 1; i < 13; i++)
            {
                bool test = false;
                int choice = 0;
                while (!test)
                {
                    choice = Random.Range(0, cards.Length);
                    test = !(cards[choice].GetComponent<Card>().Init);
                }
                cards[choice].GetComponent<Card>().CardValue = i;
                cards[choice].GetComponent<Card>().Init = true;
            }
        }
        foreach (GameObject c in cards)
        {
            c.GetComponent<Card>().setupGraphics();
        }
        if (!init)
        {
            init = true;
        }
    }

    public Sprite getCardBack()
    {
        return cardBack;
    }
    public Sprite getCardFace(int i)
    {
        return cardFace[i - 1];
    }

    void checkCards()
    {
        Debug.Log("Check Cards Firing");
        List<int> c = new List<int>();
        for (int i = 0; i < cards.Length; i++)
        {
            if (cards[i].GetComponent<Card>().State == 1)
            {
                c.Add(i);
                onlyOnce = true;
            }
            if (c.Count == 2)
            {
                if (onlyOnce)
                {
                    cardComparison(c);
                    onlyOnce = false;
                }

            }
        }
    }
    void cardComparison(List<int> c)
    {
        int x = 0;

        Debug.Log("Card Comparison Running");
        Card.doNot = true;
        if (cards[c[0]].GetComponent<Card>().CardValue == cards[c[1]].GetComponent<Card>().CardValue)
        {
            x = 2;
            matches--;
            Debug.Log("Matches: " + matches);
            MatchText.text = "Number Of Matches: " + matches;

            if (matches == 0)
            {
                //TEMPORARY PLEASE DO NOT LEAVE THIS IN MAKE SOMETHING NICE
                SceneManager.LoadScene(0);
            }

        }

        for (int i = 0; i < c.Count; i++)
        {
            cards[c[i]].GetComponent<Card>().State = x;
            cards[c[i]].GetComponent<Card>().falseCheck();
        }

    }
    //this is hard Code and wont be Production Code;
    public void LoadAllCards()
    {
        int GameOnePairTotal = 6;
        
    }
}
