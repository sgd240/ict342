﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public Texture2D cardOne;
    public Texture2D cardTwo;
    public Texture2D cardThree;
    public Texture2D cardFour;
    public Texture2D cardFive;
    public Texture2D cardSix;

    private Sprite cardOneS;
    private Sprite cardTwoS;
    private Sprite cardThreeS;
    private Sprite cardFourS;
    private Sprite cardFiveS;
    private Sprite cardSixS;

    //https://www.youtube.com/watch?v=AaQltIC6jqg&index=4&list=PLbCx65TBvT-RHOTE_Lt7iTAev-4Bf84Fu
    public Sprite[] cardFace;
    public Sprite cardBack;
    public GameObject[] cards;

    bool onlyOnce;
    public Text MatchText;
    private bool init = false;
    private int matches = 6;
    float cooldown = .5f;
    SpriteRenderer sr;
    private void Start()
    {
        sr = gameObject.AddComponent<SpriteRenderer>() as SpriteRenderer;
        LoadAllCards();
        TexturesToSprites();
    }
    public void TexturesToSprites()
    {
        float reso = 200;
        float pixelPunit = 2048;
        cardOneS = Sprite.Create(cardOne, new Rect(0.0f, 0.0f, cardOne.width, cardOne.height), new Vector2(0.5f, 0.5f), pixelPunit);
        cardTwoS = Sprite.Create(cardTwo, new Rect(0.0f, 0.0f, cardTwo.width, cardTwo.height), new Vector2(0.5f, 0.5f), pixelPunit);
        cardThreeS = Sprite.Create(cardThree, new Rect(0.0f, 0.0f, cardThree.width, cardThree.height), new Vector2(0.5f, 0.5f), pixelPunit);
        cardFourS = Sprite.Create(cardFour, new Rect(0.0f, 0.0f, cardFour.width, cardFour.height), new Vector2(0.5f, 0.5f), pixelPunit);
        cardFiveS = Sprite.Create(cardFive, new Rect(0.0f, 0.0f, cardFive.width, cardFive.height), new Vector2(0.5f, 0.5f), pixelPunit);
        cardSixS = Sprite.Create(cardSix, new Rect(0.0f, 0.0f, cardSix.width, cardSix.height), new Vector2(0.5f, 0.5f), pixelPunit);
        cardFace[0] = cardOneS;
        cardFace[1] = cardTwoS;
        cardFace[2] = cardThreeS;
        cardFace[3] = cardFourS;
        cardFace[4] = cardFiveS;
        cardFace[5] = cardSixS;
    }
    void Update () {
       
        if (!init)
        {
            initializeCards();
        }
        cooldown -= Time.deltaTime;
        if (Input.GetMouseButtonUp(0))
        {
          checkCards();
        }
	}
    void initializeCards()
    {
        for (int id = 0; id < 2; id++)
        {
            for (int i = 1; i < 7; i++)
            {
                bool test = false;
                int choice = 0;
                while (!test)
                {
                    choice = Random.Range(0, cards.Length);
                    test = !(cards[choice].GetComponent<Card>().Init);
                }
                cards[choice].GetComponent<Card>().CardValue = i;
                cards[choice].GetComponent<Card>().Init = true;
            }
        }
        foreach (GameObject c in cards)
        {
            c.GetComponent<Card>().setupGraphics();
        }
        if (!init)
        {
            init = true;
        }
    }

    public Sprite getCardBack()
    {
        return cardBack;
    }
    public Sprite getCardFace(int i)
    {
        return cardFace[i - 1];
    }

    void checkCards()
    {
        Debug.Log("Check Cards Firing");
        List<int> c = new List<int>();
        for (int i = 0; i < cards.Length; i++)
        {
            if (cards[i].GetComponent<Card>().State == 1)
            {
                c.Add(i);
                onlyOnce = true;
            }
            if (c.Count == 2)
            {
                if (onlyOnce)
                {
                    cardComparison(c);
                    onlyOnce = false;
                }

            }
        }
    }
    void cardComparison(List<int> c)
    {
        int x = 0;
        
            Debug.Log("Card Comparison Running");
            Card.doNot = true;
            if (cards[c[0]].GetComponent<Card>().CardValue == cards[c[1]].GetComponent<Card>().CardValue)
            {
                x = 2;
                matches--;
                Debug.Log("Matches: " + matches);
                MatchText.text = "Number Of Matches: " + matches;

                if (matches == 0)
                {
                    //TEMPORARY PLEASE DO NOT LEAVE THIS IN MAKE SOMETHING NICE
                    SceneManager.LoadScene(0);
                }
            
        }

        for (int i = 0; i < c.Count; i++)
        {
            cards[c[i]].GetComponent<Card>().State = x;
            cards[c[i]].GetComponent<Card>().falseCheck();
        }
    
    }
    public void LoadAllCards()
    {
        
        if (cardOne == null)
        {
            Debug.Log("trying to load One");
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(Application.dataPath + "/RawImage");
            var loadedTex = new Texture2D(60, 60);
            
            loadedTex.LoadImage(bytes);
            loadedTex.Apply();
            cardOne = loadedTex;
        }

        if (cardTwo == null)
        {
            Debug.Log("trying to load One");
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(Application.dataPath + "/RawImage (1)");
            var loadedTex = new Texture2D(60, 60);
            loadedTex.LoadImage(bytes);
            loadedTex.Apply();
            cardTwo = loadedTex;
        }
        if (cardThree == null)
        {
            Debug.Log("trying to load One");
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(Application.dataPath + "/RawImage (2)");
            var loadedTex = new Texture2D(60, 60);
            loadedTex.LoadImage(bytes);
            loadedTex.Apply();
            cardThree = loadedTex;
        }


        if (cardFour == null)
        {
            Debug.Log("trying to load One");
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(Application.dataPath + "/RawImage (3)");
            var loadedTex = new Texture2D(60, 60);
            loadedTex.LoadImage(bytes);
            loadedTex.Apply();
            cardFour = loadedTex;
        }
        if (cardFive == null)
        {
            Debug.Log("trying to load One");
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(Application.dataPath + "/RawImage (4)");
            var loadedTex = new Texture2D(60, 60);
            loadedTex.LoadImage(bytes);
            loadedTex.Apply();
            cardFive = loadedTex;
        }

        if (cardSix == null)
        {
            Debug.Log("trying to load One");
            byte[] bytes;
            bytes = System.IO.File.ReadAllBytes(Application.dataPath + "/RawImage (5)");
            var loadedTex = new Texture2D(60, 60);
            loadedTex.LoadImage(bytes);
            loadedTex.Apply();
            cardSix = loadedTex;
        }
    }
}
