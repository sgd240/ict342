﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using Random = UnityEngine.Random;
public class SaveLoadName : MonoBehaviour {

    // this script gathers all the cards that have pictures in the photo library
    // because the textures are already saved, this script will pick at random 
    // 12,24,36 names from the array of game objects and save the names which is the file path.
    
    [SerializeField]
    protected string saveTheNameOne;
    [SerializeField]
    protected string saveTheNameTwo;
    [SerializeField]
    protected string saveTheNameThree;
    [SerializeField]
    protected string saveTheNameFour;
    [SerializeField]
    protected string saveTheNameFive;
    [SerializeField]
    protected string saveTheNameSix;
    [SerializeField]
    protected string saveTheNameSeven;
    [SerializeField]
    protected string saveTheNameEight;
    [SerializeField]
    protected string saveTheNameNine;
    [SerializeField]
    protected string saveTheNameTen;
    [SerializeField]
    protected string saveTheNameEleven;
    [SerializeField]
    protected string saveTheNameTwelve;
    GameObject[] CardsWithTexture;
    int GameOneMemory = 12;
    int GameTwoMemory = 24;
    int GameThreeMemory = 36;
	void Start () {
		
	}

    private void Update()
    {
        
    }
    public void SaveNameGameOne()
    {
        CardsWithTexture = GameObject.FindGameObjectsWithTag("HasImage");
        for (int i = 0; i < GameOneMemory; i++)
        {

            int index = Random.Range(0, CardsWithTexture.Length);
            GameObject currentCard = CardsWithTexture[index];
            if (string.IsNullOrEmpty(saveTheNameOne))
            {
                Debug.Log("Loading In The First Name");
                currentCard.name = saveTheNameOne;
            }
            else if (string.IsNullOrEmpty(saveTheNameTwo))
            {
                Debug.Log("Loading In the 2nd Name");
                currentCard.name = saveTheNameTwo;
            }
            else if (string.IsNullOrEmpty(saveTheNameThree))
            {
                Debug.Log("Loading In the 3rd Name");
                currentCard.name = saveTheNameThree;
            }
            else if (string.IsNullOrEmpty(saveTheNameFour))
            {
                Debug.Log("Loading In the 4th Name");
                currentCard.name = saveTheNameFour;
            }
            else if (string.IsNullOrEmpty(saveTheNameFive))
            {
                Debug.Log("Loading In the 5th Name");
                currentCard.name = saveTheNameFive;
            }
            else if (string.IsNullOrEmpty(saveTheNameSix))
            {
                Debug.Log("Loading In the 6th Name");
                currentCard.name = saveTheNameSix;
            }
        }
    }
    public void SaveNameGameTwo()
    {
        CardsWithTexture = GameObject.FindGameObjectsWithTag("HasImage");
        for (int i = 0; i < GameOneMemory; i++)
        {

            int index = Random.Range(0, CardsWithTexture.Length);
            GameObject currentCard = CardsWithTexture[index];
            if (string.IsNullOrEmpty(saveTheNameOne))
            {
                Debug.Log("Loading In The First Name");
                currentCard.name = saveTheNameOne;
            
            }
            else if (string.IsNullOrEmpty(saveTheNameTwo))
            {
                Debug.Log("Loading In the 2nd Name");
                currentCard.name = saveTheNameTwo;
                
            }
            else if (string.IsNullOrEmpty(saveTheNameThree))
            {
                Debug.Log("Loading In the 3rd Name");
                currentCard.name = saveTheNameThree;
               
            }
            else if (string.IsNullOrEmpty(saveTheNameFour))
            {
                Debug.Log("Loading In the 4th Name");
                currentCard.name = saveTheNameFour;
            }
            else if (string.IsNullOrEmpty(saveTheNameFive))
            {
                Debug.Log("Loading In the 5th Name");
                currentCard.name = saveTheNameFive;
            }
            else if (string.IsNullOrEmpty(saveTheNameSix))
            {
                Debug.Log("Loading In the 6th Name");
                currentCard.name = saveTheNameSix;
            }
            else if (string.IsNullOrEmpty(saveTheNameSeven))
            {
                Debug.Log("Loading In the 7th Name");
                currentCard.name = saveTheNameSeven;
            }
            else if (string.IsNullOrEmpty(saveTheNameEight))
            {
                Debug.Log("Loading In the 8th Name");
                currentCard.name = saveTheNameEight;
            }
            else if (string.IsNullOrEmpty(saveTheNameNine))
            {
                Debug.Log("Loading In the 9th Name");
                currentCard.name = saveTheNameNine;
            }
            else if (string.IsNullOrEmpty(saveTheNameTen))
            {
                Debug.Log("Loading In the 10th Name");
                currentCard.name = saveTheNameTen;
            }
            else if (string.IsNullOrEmpty(saveTheNameEleven))
            {
                Debug.Log("Loading In the 11th Name");
                currentCard.name = saveTheNameEleven;
            }
            else if (string.IsNullOrEmpty(saveTheNameTwelve))
            {
                Debug.Log("Loading In the 12th Name");
                currentCard.name = saveTheNameTwelve;
            }
          


        }
        
    }
    public string SaveTheNameOne
    {
        get { return saveTheNameOne; }
        set { saveTheNameOne = value; }
    }
    public string SaveTheNameTwo
    {
        get { return saveTheNameTwo; }
        set { saveTheNameTwo = value; }
    }
    public string SaveTheNameThree
    {
        get { return saveTheNameThree; }
        set { saveTheNameThree = value; }
    }
    public string SaveTheNameFour
    {
        get { return saveTheNameFour; }
        set { saveTheNameFour = value; }
    }
    public string SaveTheNameFive
    {
        get { return saveTheNameFive; }
        set { saveTheNameFive = value; }
    }
    public string SaveTheNameSix
    {
        get { return saveTheNameSix; }
        set { saveTheNameSix = value; }
    }
    public string SaveTheNameSeven
    {
        get { return saveTheNameSeven; }
        set { saveTheNameSeven = value; }
    }
    public string SaveTheNameEight
    {
        get { return saveTheNameEight; }
        set { saveTheNameEight = value; }

    }
    public string SaveTheNameNine
    {
        get { return saveTheNameTen; }
        set { saveTheNameTen = value; }
    }
    public string SaveTheNameEleven
    {
        get { return saveTheNameEleven; }
        set { saveTheNameEleven = value; }
    }
    public string SaveTheNameTwelve
    {
        get { return saveTheNameTwelve; }
        set { saveTheNameTwelve = value; }
    }
    public void SaveGameOneTiles()
    {
        
    }
}

[Serializable]
class MemoryGameTwelve
{
    public string SaveTheNameOne;
    public string SaveTheNameTwo;
    public string SaveTheNameThree;
    public string SaveTheNameFour;
    public string SaveTheNameFive;
    public string SaveTheNameSix;

}
[Serializable]
class MemoryGameTwentyFour
{
    public string SaveTheNameOne;
    public string SaveTheNameTwo;
    public string SaveTheNameThree;
    public string SaveTheNameFour;
    public string SaveTheNameFive;
    public string SaveTheNameSix;
    public string SaveTheNameSeven;
    public string SaveTheNameEight;
    public string SaveTheNameNine;
    public string SaveTheNameTen;
    public string SaveTheNameEleven;
    public string SaveTheNameTwelve;

}
[Serializable]
class MemoryGameThirtySix
{
    public string SaveTheNameOne;
    public string SaveTheNameTwo;
    public string SaveTheNameThree;
    public string SaveTheNameFour;
    public string SaveTheNameFive;
    public string SaveTheNameSix;
    public string SaveTheNameSeven;
    public string SaveTheNameEight;
    public string SaveTheNameNine;
    public string SaveTheNameTen;
    public string SaveTheNameEleven;
    public string SaveTheNameTwelve;
    public string SaveTheNameThirteen;
    public string SaveTheNameFourteen;
    public string SaveTheNameFifthteen;
    public string SaveTheNameSixteen;
    public string SaveTheNameSeventeen;
    public string SaveTheNameEighteen;

}
